/*
CONTROLS:
1-4: toggle algorithms
r: restart solving current problem
n: new problem
x: switch mode
up: increment mutationrate
down: decrement mutationrate
*/

int mutationrate = 2;
int popsize = 500;
int geneamount = 35;
String type = "circle";

boolean a = true;
boolean b = true;
boolean c = true;
boolean d = true;
Sys test = new Sys(type, geneamount, popsize);
//test.mateTest();
void setup() {
  size(1200, 700);
  frameRate(100);
  smooth();
}

void draw() {
  background(0);
  test.showInfo();
  text("mutation rate: " + mutationrate, 800, 380);

  //mutate population. select best species. clone best to make new population. Old population dies.
  if (a) test.mutation(mutationrate);

  //whole population mates. best with next-best, second-best with next-second-best, etc. Mutate offsprings. Bad offsprings and parents die.
  if (b) test.crossoverMutation(mutationrate);

  //best mate with all(including itself) and creates new population. Bad offsprings die.
  if (c) test.crossoverMutation2(mutationrate);

  //best mate with next-best and creates new population which mutates. Bad offsprings die.
  if (d) test.crossoverMutation3(mutationrate);
}

void keyReleased() {
  switch(key) {
  case '1': 
    if (a) a=false; 
    else a=true; 
    break;
  case '2': 
    if (b) b=false; 
    else b=true; 
    break;
  case '3': 
    if (c) c=false; 
    else c=true; 
    break;
  case '4': 
    if (d) d=false; 
    else d=true; 
    break;
  case 'r': 
    test.restart(); 
    break;
  case 'n': 
    test.newGenes(); 
    break;
  case 'x': 
    test.sType(); 
    break;
  }
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      mutationrate++;
    } 
    else if (keyCode == DOWN) {
      mutationrate--;
    }
  }
}

class Stat {

  LinkedList<Point> points = new LinkedList<Point>();

  public void draw(int col) {
    strokeWeight(1);
    switch(col) {
    case 0: 
      stroke(256, 0, 0); 
      break;
    case 1: 
      stroke(0, 256, 0); 
      break;
    case 2: 
      stroke(0, 0, 256); 
      break;
    case 3: 
      stroke(256, 256, 0); 
      break;
    }
    ListIterator<Point> itr = points.listIterator();

    int[] prev = null;
    int[] next = null;
    int[] first = null;

    Point current;

    if (itr.hasNext()) {
      current = itr.next();
      current.draw();
      prev = current.getCords();
      first = prev;
    }

    while (itr.hasNext ()) {
      current = itr.next();
      next = current.getCords();
      line(prev[0], prev[1], next[0], next[1]);
      current.draw();
      prev = next;
    }
  }

  public void add(int y) {
    points.add(new Point(y));
  }
}

class Point {
  private int x;
  private int y;

  Point(int y) {
    this.y = y;
    this.x = 800;
  }

  public int[] getCords() {
    int[] cords = {
      x, y
    };
    return cords;
  }

  public void draw() {
    x++;
    //fill(0,0,0);
    //ellipse(x,y, 10, 10);
  }
}

class CreatureCompare implements Comparator<Creature> {
  public int compare(Creature a, Creature b) {
    double aS = a.score();
    double bS = b.score();

    if (aS == bS)
      return 0;
    else if (aS < bS)
      return -1;
    else
      return 1;
  }
}

class Gene {
  public final int id;
  private int x;
  private int y;
  private Random rnd = new Random();

  Gene(int id, int x, int y) {
    this.id = id;
    this.x = x;
    this.y = y;
    //gen random
    //x = rnd.nextInt(650)+1;
    //y = rnd.nextInt(640)+1;
  }

  public int[] getCords() {
    int[] cords = {
      x, y
    };
    return cords;
  }

  public void draw(int col) {
    switch(col) {
    case 0:    
      fill(256, 0, 0); 
      break;
    case 1:    
      fill(0, 256, 0); 
      break;
    case 2:    
      fill(0, 0, 256); 
      break;
    case 3:    
      fill(256, 256, 0); 
      break;
    }
    ellipse(x, y, 10, 10);
  }
}

class Sys {
  private int geneNum;
  private int creatureNum;
  private LinkedList<Gene> genes = new LinkedList<Gene>();
  private ArrayList<Creature> populationa = new ArrayList<Creature>();
  private ArrayList<Creature> populationb = new ArrayList<Creature>();
  private ArrayList<Creature> populationc = new ArrayList<Creature>();
  private ArrayList<Creature> populationd = new ArrayList<Creature>();
  private boolean exit = false;
  private int generation = 0;
  private Stat aStat = new Stat();
  private Stat bStat = new Stat();
  private Stat cStat = new Stat();
  private Stat dStat = new Stat();

  Sys(String type, int geneNum, int creatureNum) {
    this.geneNum = geneNum;
    this.creatureNum = creatureNum;
    Random rnd = new Random();

    if (type.equals("circle")) {
      int xres = 650;
      int yres = 650;
      int r = 300;

      for (int i=0;i<geneNum;i++) {
        int a = rnd.nextInt(360);
        int xb = (int) (xres/2 + r * cos(a));
        int yb = (int) (yres/2 + r * sin(a));
        genes.add(new Gene(i, xb, yb));
      }
    }
    else {
      for (int i=0;i<geneNum;i++) {
        genes.add(new Gene(i, rnd.nextInt(650)+1, rnd.nextInt(650)+1));
      }
    }

    for (int i=0;i<creatureNum;i++) {
      Collections.shuffle(genes);
      this.populationa.add(new Creature(genes));
      this.populationb.add(new Creature(genes));
      this.populationc.add(new Creature(genes));
      this.populationd.add(new Creature(genes));
    }
  }
  public void sType() {
    if (type.equals("circle")) {
      type = "no_circle";
      newGenes();
    }
    else {
      type = "circle";
      newGenes();
    }
  }

  public void newGenes() {
    genes.clear();  	
    Random rnd = new Random();
    if (type.equals("circle")) {
      int xres = 650;
      int yres = 650;
      int r = 300;

      for (int i=0;i<geneNum;i++) {
        int a = rnd.nextInt(360);
        int xb = (int) (xres/2 + r * cos(a));
        int yb = (int) (yres/2 + r * sin(a));
        genes.add(new Gene(i, xb, yb));
      }
    }
    else {
      for (int i=0;i<geneNum;i++) {
        genes.add(new Gene(i, rnd.nextInt(650)+1, rnd.nextInt(650)+1));
      }
    }
    restart();
  }

  public void restart() {
    generation = 0;
    this.populationa.clear();
    this.populationb.clear();
    this.populationc.clear();
    this.populationd.clear();

    for (int i=0;i<creatureNum;i++) {
      Collections.shuffle(genes);
      this.populationa.add(new Creature(genes));
      this.populationb.add(new Creature(genes));
      this.populationc.add(new Creature(genes));
      this.populationd.add(new Creature(genes));
    }
  }

  public void showInfo() {
    fill(256, 256, 256);
    text("Population size: " + creatureNum + " Number of genes: " + geneNum, 800, 360); 
    text("Generation: " + generation++, 800, 370);
  }

  public void mutation(int mutationrate) {

    /*

     		make population. mutate population. select best species. clone best to make new population.
     
     		*/
    //ArrayList<Creature> population = new ArrayList<Creature>(this.population);
    CreatureCompare compare = new CreatureCompare();

    //println("\nMutation:\n");
    //f//or(int i = 0; i<generations;i++){
    Collections.sort(populationa, compare);
    ArrayList<Creature> currentGeneration = new ArrayList<Creature>();

    //println(population.get(0).score() + " " + population.size());

    fill(256, 0, 0);
    double currscore = populationa.get(0).score();
    text("" + currscore, 800, 300);
    aStat.add((int) (currscore/geneNum));
    aStat.draw(0);
    populationa.get(0).draw(0);

    for (int b=0;b<creatureNum-1;b++) {
      currentGeneration.add(new Creature(new LinkedList(populationa.get(0).getGenes())));
    }

    for (Creature creature : currentGeneration) {
      for (int i=0;i<mutationrate;i++)
        creature.mutate();
    }

    populationa.subList(1, populationa.size()).clear();
    populationa.addAll(currentGeneration);
    currentGeneration.clear();
    //}
    //population.get(0).draw();
    //println(population.get(0).score());
  }

  public void crossoverMutation(int mutationrate) {
    /*
	
     		whole population mates. best with next-best, second-best with next-second-best, etc.
     	
     		*/
    //ArrayList<Creature> population = new ArrayList<Creature>(this.population);
    CreatureCompare compare = new CreatureCompare();

    //println("\nCrossover and Mutation:\n");
    //for(int i = 0; i<generations;i++){
    //find parents(two best creatures)
    ArrayList<Creature> currentGeneration = new ArrayList<Creature>();

    Collections.sort(populationb, compare);
    populationb.subList(creatureNum, populationb.size()).clear();

    //println(population.get(0).score() + " " + population.size());

    fill(0, 256, 0);
    double currscore = populationb.get(0).score();
    text("" + currscore, 800, 310);
    bStat.add((int) (currscore/geneNum));
    bStat.draw(1);
    populationb.get(0).draw(1);

    Iterator<Creature> itr = populationb.iterator();
    Creature prev = itr.next();
    Creature next = null;

    while (itr.hasNext ()) {
      next = itr.next();
      currentGeneration.addAll(makeChild(prev, next));
    }

    Collections.sort(currentGeneration, compare);

    for (Creature creature : currentGeneration) {
      for (int i=0;i<mutationrate;i++)
        creature.mutate();
    }

    currentGeneration.subList(creatureNum, currentGeneration.size()).clear();
    Collections.sort(currentGeneration, compare);

    populationb.addAll(currentGeneration);
    currentGeneration.clear();

    //}
    //population.get(0).draw();
    //println(population.get(0).score());
  }

  public void crossoverMutation2(int mutationrate) {
    /*
		
     		best mate with all(including itself) and creates new population.
     		
     		*/
    //ArrayList<Creature> population = new ArrayList<Creature>(this.population);
    CreatureCompare compare = new CreatureCompare();

    //println("\nCrossover and Mutation 2:\n");
    //for(int i = 0; i<generations;i++){
    ArrayList<Creature> currentGeneration = new ArrayList<Creature>();

    Collections.sort(populationc, compare);
    populationc.subList(creatureNum, populationc.size()).clear();

    //println(population.get(0).score() + " " + population.size());

    fill(0, 0, 256);
    double currscore = populationc.get(0).score();
    text("" + currscore, 800, 320);
    cStat.add((int) (currscore/geneNum));
    cStat.draw(2);
    populationc.get(0).draw(2);

    for (Creature z : populationc) {
      currentGeneration.addAll(makeChild(populationc.get(0), z));
    }

    Collections.sort(currentGeneration, compare);

    for (Creature creature : currentGeneration) {
      for (int i=0;i<mutationrate;i++)
        creature.mutate();
    }

    currentGeneration.subList(creatureNum, currentGeneration.size()).clear();
    Collections.sort(currentGeneration, compare);

    populationc.addAll(currentGeneration);
    currentGeneration.clear();
    //}
    //population.get(0).draw();
    //println(population.get(0).score());
  }

  public void crossoverMutation3(int mutationrate) {
    /*
		
     		best mate with next-best and creates new population.
     		
     		*/
    //ArrayList<Creature> population = new ArrayList<Creature>(this.population);
    CreatureCompare compare = new CreatureCompare();

    //println("\nCrossover and Mutation 3:\n");
    ////for(int i = 0; i<generations;i++){
    ArrayList<Creature> currentGeneration = new ArrayList<Creature>();

    Collections.sort(populationd, compare);
    populationd.subList(creatureNum, populationd.size()).clear();

    //println(population.get(0).score() + " " + population.size());

    fill(256, 256, 0);
    double currscore = populationd.get(0).score();
    text("" + currscore, 800, 330);
    dStat.add((int) (currscore/geneNum));
    dStat.draw(3);
    populationd.get(0).draw(3);

    for (int s=0;s<populationd.size();s++) {
      currentGeneration.addAll(makeChild(populationd.get(0), populationd.get(1)));
    }

    Collections.sort(currentGeneration, compare);

    for (Creature creature : currentGeneration) {
      for (int i=0;i<mutationrate;i++)
        creature.mutate();
    }

    currentGeneration.subList(creatureNum, currentGeneration.size()).clear();
    Collections.sort(currentGeneration, compare);

    populationd.addAll(currentGeneration);
    currentGeneration.clear();

    //}
    //population.get(0).draw();
    //println(population.get(0).score());
  }

  public ArrayList<Creature> makeChild(Creature m, Creature f) {		
    LinkedList<Gene> mGenes = m.getGenes();
    LinkedList<Gene> fGenes = f.getGenes();
    LinkedList<Gene> childGenes = new LinkedList<Gene>();
    LinkedList<Gene> childGenes2 = new LinkedList<Gene>();
    Random rnd = new Random();
    int split = 1 + rnd.nextInt(geneNum);

    for (int i=0;i<geneNum;i++) {
      if (i <= split) {
        childGenes.add(mGenes.get(i));
        childGenes2.add(fGenes.get(i));
      }
      else {
        childGenes.add(fGenes.get(i));
        childGenes2.add(mGenes.get(i));
      }
    }

    ArrayList<Gene> nf1 = new ArrayList<Gene>();
    ArrayList<Gene> nf2 = new ArrayList<Gene>();


    for (Gene gene : genes) {
      if (!childGenes.contains(gene)) {
        nf1.add(gene);
      }
      if (!childGenes2.contains(gene)) {
        nf2.add(gene);
      }
    }

    if (nf1.size() > 0 && nf2.size() > 0) {
      for (Gene gene : nf2) {
        childGenes.set(childGenes.indexOf(gene), nf1.get(nf2.indexOf(gene)));
      }
      for (Gene gene : nf1) {
        childGenes2.set(childGenes2.indexOf(gene), nf2.get(nf1.indexOf(gene)));
      }
    }

    ArrayList<Creature> offsprings = new ArrayList<Creature>();

    offsprings.add(new Creature(childGenes));
    offsprings.add(new Creature(childGenes2));

    return offsprings;
  }
}

class Creature {
  private LinkedList<Gene> genes = new LinkedList<Gene>();

  Creature(LinkedList<Gene> genes) {
    for (Gene gene : genes) {
      this.genes.add(gene);
    }
  }

  public void mutate() {
    Random rnd = new Random();
    Collections.swap(genes, rnd.nextInt(genes.size()), rnd.nextInt(genes.size()));
  }

  public void printGenes() {

    for (Gene gene : genes) {
      //printf("%d|",gene.id);
    }

    println(score());
  }

  public LinkedList<Gene> getGenes() {
    return genes;
  }

  public double score() {
    ListIterator<Gene> itr = genes.listIterator();

    double score = 0;
    int[] prev = null;
    int[] next = null;
    int[] first = null;

    if (itr.hasNext()) {
      prev = itr.next().getCords();
      first = prev;
    }

    while (itr.hasNext ()) {
      next = itr.next().getCords();
      score += Math.sqrt((next[0]-prev[0])*(next[0]-prev[0]) + (next[1]-prev[1])*(next[1]-prev[1]));
      prev = next;
    }

    next = prev;
    prev = first;
    score += Math.sqrt((next[0]-prev[0])*(next[0]-prev[0]) + (next[1]-prev[1])*(next[1]-prev[1]));

    return score;
  }
  public void draw(int col) {
    switch(col) {
    case 0:    
      strokeWeight(8); 
      stroke(128, 0, 0); 
      break;
    case 1:    
      strokeWeight(6); 
      stroke(0, 128, 0); 
      break;
    case 2:    
      strokeWeight(4); 
      stroke(0, 0, 128); 
      break;
    case 3:    
      strokeWeight(2); 
      stroke(128, 128, 0); 
      break;
    }
    ListIterator<Gene> itr = genes.listIterator();

    int[] prev = null;
    int[] next = null;
    int[] first = null;

    Gene current;

    if (itr.hasNext()) {
      current = itr.next();
      current.draw(col);
      prev = current.getCords();
      first = prev;
    }

    while (itr.hasNext ()) {
      current = itr.next();
      next = current.getCords();
      line(prev[0], prev[1], next[0], next[1]);
      current.draw(col);
      prev = next;
    }

    next = prev;
    prev = first;
    line(prev[0], prev[1], next[0], next[1]);
  }
}

