ArrayList<bullet> bullets = new ArrayList<bullet>();

void setup(){
  size(200,200);
}

void draw(){
  background(51);
  
  bullets.add(new bullet(width/2,height/2,PI));
  
  for(int i=0;i<bullets.size();i++){
    if(bullets.get(i).out){
      bullets.remove(i);
    }else{
      bullets.get(i).move();
      bullets.get(i).draw();
    }
  }
}

class bullet {
  int x;
  int y;
  int sx;
  int sy;
  int pos;
  int speed = 5;
  float angle;
  boolean out = false;
  
  bullet(int setX,int setY, float setAngle){
    x = setX;
    y = setY;
    angle = setAngle;
  }
  
  void move(){
    if(x < width && y < height){
      x = sx + pos * (int) sin(angle);
      y = sy + pos * (int) cos(angle);
      pos += speed;
    }else{
      out = true;
    }
  }
  
  void draw(){
    ellipse(x,y,10,10);
  }
  
}
