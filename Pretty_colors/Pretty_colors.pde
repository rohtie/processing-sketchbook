int times;
int times2;

void setup(){
  size(256,256);
  frameRate(30);
}

void draw(){
  background(0);
  for(int x=0;x<256;x++){
    for(int y=0;y<256;y++){
      switch(times2){
       default:
         stroke(y,times,x);
         break;
       case 1:
         stroke(times,y,x);
         break;
       case 2:
         stroke(y,x,times);
         break;
      }
      point(x,y);
    }
  }
  times+=2;
  //fill(256,256,256);
  //text(times,0,250);
  if(times == 256){
    if(times2 == 2)
      times2 = 0;
    else
      times2++;
    times = 0;
  }
    
}
