ArrayList<Solution> solutions = new ArrayList<Solution>();
int generation = 0;
int mutationRate = 20;
int numSolutions = 500;
int nodes = 25;

Problem TSP = new Problem(nodes);

// TODO
//start - high mutation
//end - low mutation
//different species: best, 2nd best, 3rd best, etc...

Solution bestSolution;

void setup(){
  bestSolution = new Solution(TSP);
  for(int i=0;i<numSolutions;i++){
    solutions.add(new Solution(TSP));
  }
  
  bestSolution = solutions.get(0);
  
  size(500, 500);
  frameRate(10);
  smooth();
}

void draw(){
  background(102);
  
  bestSolution.draw();
  for(Solution solution : solutions){
    if((solution.score()) < (bestSolution.score())){
      bestSolution = solution;
    }
  }
  //if(generation % 5 == 0 && mutationRate > 1) mutationRate--;
  for(Solution solution : solutions){
    if(solution != bestSolution){
      solution.clone(bestSolution.getClone());
     //for(int i=0;i<mutationRate;i++)
        solution.mutate();
      }
  }
  
  fill(256,256,256);
  text("Nodes:" + TSP.getNodes().size(), 20, 470);
  text("Generation:" + generation, 20, 480);
  text("Score:" + bestSolution.score(), 20, 490);
  generation++;
}

class Node {
  private int id;
  private int x;
  private int y;
  private int size = 5;
  
  Node(int x, int y, int id){
    this.x = x;
    this.y = y;
    this.id = id;
  }
  
  public int[] getCords(){
    int[] cords = {x,y};
    return cords;
  }
  
  public void draw(){
    fill(128,0,0);
    ellipse(x,y, size, size);
    //fill(256,256,256);
    //text("" + id ,x,y);
  }
}

class Solution {
  private LinkedList<Node> nodes = new LinkedList<Node>();
  private ListIterator<Node> itr;
  
  Solution(Problem problem){
    ArrayList<Node> nodes = problem.getNodes();
    
    Collections.shuffle(nodes);
    
    for(Node node : nodes){
      this.nodes.add(node);
    }
  }
  
  public LinkedList<Node> getClone(){
    return this.nodes;
  }
  
  public void clone(LinkedList<Node> nodes){
    itr = nodes.listIterator();
    this.nodes.clear();
    while(itr.hasNext()){
      this.nodes.add(itr.next());
    }
  }
  
  public void mutate(){
    //move random node to random place
    Random rnd = new Random();
    
    int chosen = rnd.nextInt(nodes.size());
    Node n = nodes.get(chosen);
    nodes.remove(chosen);
    
    int newPlacement = rnd.nextInt(nodes.size());
    while(chosen == newPlacement)
      newPlacement = rnd.nextInt(nodes.size());
    
    nodes.add(newPlacement,n);
  }
  
  public double score(){
    //get total score
    itr = nodes.listIterator();
    double score = 0;
    int[] prev = null;
    int[] next = null;
    int[] first = null;
    
    if(itr.hasNext()){
      prev =  itr.next().getCords();
      first = prev;
    }
    
    while(itr.hasNext()){
      next = itr.next().getCords();
      score += Math.sqrt((next[0]-prev[0])*(next[0]-prev[0]) + (next[1]-prev[1])*(next[1]-prev[1]));
      prev = next;
    }
    next = prev;
    prev = first;
    score += Math.sqrt((next[0]-prev[0])*(next[0]-prev[0]) + (next[1]-prev[1])*(next[1]-prev[1]));
    
    return score;    
  }
  
  public void draw(){
    itr = nodes.listIterator();
    

    int[] prev = null;
    int[] next = null;
    int[] first = null;
    
    Node current;
    
    if(itr.hasNext()){
      current = itr.next();
      current.draw();
      prev = current.getCords();
      first = prev;
    }
    
    while(itr.hasNext()){
      current = itr.next();
      next = current.getCords();
      stroke(64, 0, 0);
      line(prev[0],prev[1],next[0],next[1]);
      current.draw();
      prev = next;
    }
    
    next = prev;
    prev = first;
    stroke(64, 0, 0);
    line(prev[0],prev[1],next[0],next[1]);
    
    /*
    line(a.x(),a.y(),b.x(),b.y());*/
  }
}

class Problem {
  private ArrayList<Node> nodes = new ArrayList<Node>();
  private Random rnd = new Random();
  
  Problem(int nodez){
    //int node_count = 16 + rnd.nextInt(100);
    //int node_count = 100;
    for(int i=0;i<nodez;i++){
      nodes.add(new Node(rnd.nextInt(500),rnd.nextInt(500),i));
    }
  }
  
  public ArrayList<Node> getNodes(){
    return nodes;
  }
}
