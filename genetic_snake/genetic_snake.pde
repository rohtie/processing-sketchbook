int mapx = 200;
int mapy = 200;
int framerate = 10;

Snake s = new Snake(200);
Bot b = new Bot(s);

void setup(){
  size(mapx+20, mapy+30);
  frameRate(framerate);
  smooth();
  stroke(256,256,256);
}

void draw(){
  //b.move();
  frameRate(framerate);
  background(0);
  s.drawStage();
  s.drawStatus();
  s.move();
  s.draw();
}

void keyPressed() {
    switch(key) {
    case 'w':
      s.changeDir('w');
      break;
      
    case 's':
      s.changeDir('s');
      break;
      
    case 'a':
      s.changeDir('a');
      break;
      
    case 'd':
      s.changeDir('d');
      break;
    }
}

void keyReleased() {
  switch(key) { 
    case 'o':
      if(framerate - 1 != 0)
        framerate--;
      break;
      
    case 'p':
      framerate++;
      break;
      
    case 'n':
      s.addNode();
      break;
  }
}

class Node {
  private int id;
  private int x;
  private int y;
  
  Node(int x, int y,int id){
    this.x = x;
    this.y = y;
    this.id = id;
  }
  
  public int getId(){
    return this.id;
  }
  
  public void setCords(int[] cords){
    this.x = cords[0];
    this.y = cords[1];
  }
  
  public int[] getCords(){
    int[] cords = {this.x,this.y};
    return cords;
  }
  
  public void move(char key){
     switch(key) {
      case 'w':
        this.y-=10;
        break;
        
      case 's':
        this.y+=10;
        break;
        
      case 'a':
        this.x-=10;
        break;
        
      case 'd':
        this.x+=10;
        break;
    }
  }
  
  public void draw(){
    stroke(256,256,256);
    fill(128,128,128);
    ellipse(x,y, 10, 10);
    fill(256,256,256);
    //text("" + id,x-4,y+4);
  }
}

class Snake {
  ArrayList<Node> nodes = new ArrayList<Node>();
  ArrayList<Character> pattern = new ArrayList<Character>();
  char lastDir = 'a';
  char newDir = 'a';
  int lastId;
  boolean dead = false;
  Food current;
  int score = 0;
  int alive = 0;
  int fitness;
  
  Snake(int num){
    for(int i = 20; i <= (num+1)*10; i += 10){
      nodes.add(new Node(50+i,80,i/10));
      lastId = i/10;
    }
    newFood();
   }
  
  public void move(){
    switch(lastDir){
      case 'w':
        if(newDir != 's')
          lastDir = newDir;
        break;
        
      case 's':
        if(newDir != 'w')
          lastDir = newDir;
        break;
        
      case 'a':
        if(newDir != 'd')
          lastDir = newDir;
        break;
        
      case 'd':
        if(newDir != 'a')
          lastDir = newDir;
        break;
    }
    
    if(!dead){
      alive++;
      ArrayList<int[]> cords = new ArrayList<int[]>();
  
      for(Node n : nodes){
        cords.add(n.getCords());
      }
      
      nodes.get(0).move(lastDir);
      
      Iterator<int[]> itr = cords.iterator();
      Node prevN = null;
      
      for(Node n : nodes){
        if(n != nodes.get(0)){
          int[] prevCord = prevN.getCords();
          int[] cordSet = itr.next();
          
          if(!(prevCord[0] == cordSet[0] && prevCord[1] == cordSet[1]))
            n.setCords(cordSet);
        }
        prevN = n;
      }
      int colX = nodes.get(0).getCords()[0];
      int colY = nodes.get(0).getCords()[1];
      
      for(Node n : nodes){
        if(n != nodes.get(0)){
          int[] current = n.getCords();
          if(colX == current[0] && colY == current[1]){
                this.dead = true;
          }
        }
      }
      int[] head = nodes.get(0).getCords();
      int[] food = current.getCords();
      if(head[0] == food[0] && head[1] == food[1]){
        addNode();
        newFood();
      }
      if(head[0] < 20 || head[1] < 20 || head[0] > mapx || head[1] > mapy){
        dead = true;
      }
      if(dead){
       for(int i = 0; i<nodes.size();i++){
         nodes.get(i).setCords(cords.get(i));
       }
      }
    }
  }
  
  public void addNode(){
    int[] lastCords = nodes.get(nodes.size() - 1).getCords();
    nodes.add(new Node(lastCords[0],lastCords[1],++lastId));
    score++;
  }
  
  public void newFood(){
    Random rnd = new Random();
  
    int fx;
    int fy;
    
    while(true){
      fx = rnd.nextInt((mapx/10) - 1)+2;
      fy = rnd.nextInt((mapy/10) - 1)+2;
      
      fx *= 10;
      fy *= 10;
      
      boolean collide = false;
      
      for(Node n : nodes){
        int[] xy = n.getCords();
        
        if(xy[0] == fx && xy[1] == fy){
          collide = true;
          break;
        }
      }
      
      if (!collide)
        break;
    }
    current = new Food(fx,fy);
  }
  
  public void changeDir(char dir){
    newDir = dir;
  }
  
  public void draw(){
    current.draw();
    for(Node n : nodes){
      n.draw();
    }
  }
  
  public void drawStatus(){
    fitness = score*alive;
    fill(256,256,256);
    text("Score: " + score, 10,mapy+25);
    text("Alive: " + alive, 70,mapy+25);
    text("Fitness: " + fitness, 130,mapy+25);
  }
  
  public void drawStage(){
    stroke(256,256,256);
    fill(0,0,0);
    //line(mapx,mapy,0,mapy);
    rect(10,10,mapx,mapy);
  }
}

class Bot {
  ArrayList<Character> pattern = new ArrayList<Character>();
  Iterator<Character> itr;
  Random rnd = new Random();
  Snake s;
  
  Bot(Snake s){
    this.s = s;
    char[] step = {'w','s','a','d'};
    
    int stepsInPattern = 16;
    
    for(int i=0;i<stepsInPattern;i++){
      pattern.add(step[rnd.nextInt(4)]);
    }
    
    itr = pattern.iterator();
  }
  
  public void move(){
    if(itr.hasNext()){
     switch(itr.next()) {
      case 'w':
        s.changeDir('w');
        break;
        
      case 's':
        s.changeDir('s');
        break;
        
      case 'a':
        s.changeDir('a');
        break;
        
      case 'd':
        s.changeDir('d');
        break;
      }
    }else{
      itr = pattern.iterator();
      move();
    }
  }
  
  public void mutate(){
    Collections.swap(pattern, rnd.nextInt(pattern.size()), rnd.nextInt(pattern.size()));
  }
}

class Food {
  private int size;
  private int x;
  private int y;
  
  Food(int x,int y){
    this.x = x;
    this.y = y;
  }
  
  public int[] getCords(){
    int[] cords = {x,y};
    return cords;
  }
  
  public void draw(){
    stroke(256,256,256);
    fill(0,0,0);
    ellipse(x,y,10,10);
  }
}

abstract class Evolution {
  private ArrayList<Snake> population = new ArrayList<Snake>();
  
  Evolution(){
    //create population
  }
  
  public abstract void select();
  public abstract void mate();
  public abstract void mutate();
  
  public void crossover(Snake a, Snake b){
    //implement
  }
}

