/*Problem n = new Problem(1);

class Problem {
  ArrayList<Triangle> tris = new ArrayList<Triangle>();
  Random rnd = new Random();  
  
  Problem(int num){
    for(int i=0;i<num;i++){
      Point[] newpoints = new Point[3];
      for(int j=0;j<3;j++){
         newpoints[j] = new Point(rnd.nextInt(700)+1,rnd.nextInt(700)+1);
      }
      tris.add(new Triangle(newpoints[0],newpoints[1],newpoints[2]));
    }
  }
  
  public void draw(){
    for(Triangle t : tris){
      t.draw();
    }
  }
}*/

Triangle t;

void setup() {
  size(700, 700);
  //frameRate(60);
  smooth();
  Random rnd = new Random();
      Point[] newpoints = new Point[3];
      for(int j=0;j<3;j++){
         newpoints[j] = new Point(rnd.nextInt(700)+1,rnd.nextInt(700)+1);
      }
      t = new Triangle(newpoints[0],newpoints[1],newpoints[2]);
}

void draw() {
  background(0);
  t.draw();
  text("x:" + mouseX + "y:" + mouseY,500,500);
}

public class Triangle {
    public final Point a, b, c;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public void draw(){
      stroke(256,256,256);
      line(a.x,a.y,b.x,b.y);
      line(b.x,b.y,c.x,c.y);
      line(c.x,c.y,a.x,a.y);
      a.draw();
      b.draw();
      c.draw();
    }
}

public class Point {
    public final int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public void draw(){
      fill(256,256,256);
      ellipse(x,y, 2, 2);
    }
}
