boolean food = false;
int mapx = 250;
int mapy = 250;
int geneCount = 10000;
int nodeCount = 10;
int popSize = 250;
int mutationRate = 1;
int framerate = 100;



testEvolution2 s = new testEvolution2();

void setup(){
  size(mapx+20, mapy+30);
  frameRate(framerate);
  smooth();
  stroke(256,256,256);
}

void draw(){
  background(0);
  s.move();
  s.draw();
}

void keyReleased() {
  switch(key) {
  case 'x': 
    s.killLoop();
    break;
  }
}

class testEvolution extends Evolution{
  private ArrayList<Creature> pop;
  ArrayList<Creature> offsprings;
  
  testEvolution(){
    super();
    pop = super.pop;
  }
  
  public void select(){
    Collections.sort(pop);
    super.currentBest = pop.get(0).getAlive();
  }
  
  public void mate(){
    offsprings = new ArrayList<Creature>();
    Iterator<Creature> itr = pop.iterator();
    
    Creature prev = itr.next();
    Creature next = null;

    while (itr.hasNext()) {
      next = itr.next();
      offsprings.addAll(crossover(prev,next));
      
      if(itr.hasNext())
        prev = itr.next();
    }
  }
  
  public void mutate(){
    for(Creature c : offsprings){
      for(int i=0;i<mutationRate;i++)
        c.mutate();
    }
    pop.subList(1, pop.size()).clear();
    pop.addAll(offsprings);
  }
}

class testEvolution2 extends Evolution{
  private ArrayList<Creature> pop;
  ArrayList<Creature> offsprings;
  
  testEvolution2(){
    super();
    pop = super.pop;
  }
  
  public void select(){
    Collections.sort(pop);
    super.currentBest = pop.get(0).getAlive();
  }
  
  public void mate(){
    offsprings = new ArrayList<Creature>();
    Iterator<Creature> itr = pop.iterator();
    
    Creature prev = itr.next();
    Creature next = null;

    while (itr.hasNext()) {
      next = itr.next();
      offsprings.addAll(crossover(pop.get(0),next));
      
      if(itr.hasNext())
        prev = itr.next();
    }
  }
  
  public void mutate(){
    for(Creature c : offsprings){
      for(int i=0;i<mutationRate;i++)
        c.mutate();
    }
    pop.subList(1, pop.size()).clear();
    pop.addAll(offsprings);
  }
}

abstract class Evolution {
  private ArrayList<Creature> pop = new ArrayList<Creature>();
  private int currentBest;
  private boolean life;
  
  Evolution(){
    for(int i=0;i<popSize;i++){
      pop.add(new Creature(null));
    }
  }
  
  public abstract void select();
  public abstract void mate();
  public abstract void mutate();
  
  public void move(){
    life = false;
    for(Creature c : pop){
      c.move();
      if(!c.isDead())
        life = true;
    }

    if(!life){
      select();
      mate();
      mutate();
    }
  }
  
  public void draw(){
    stroke(256,256,256);
    fill(0,0,0);
    rect(10,10,mapx,mapy);
    fill(256,256,256);
    text("Fitness: " + currentBest,20,mapy+25);
    
    for(Creature c : pop){
        if(!c.isDead())
          c.draw();
    }
  }
  
  public void killLoop(){
    for(Creature c : pop){
      if(!c.isDead()){
        c.kill();
      }
    }
  }
  
  public ArrayList<Creature> crossover(Creature f, Creature m){
    ArrayList<Character> fGenes = f.getGenes();
    ArrayList<Character> mGenes = m.getGenes();
    
    ArrayList<Character> childGenes = new ArrayList<Character>();
    ArrayList<Character> childGenes2 = new ArrayList<Character>();
    
    Random rnd = new Random();
    int split = 1 + rnd.nextInt(geneCount);

    for (int i=0;i<geneCount;i++) {
      if (i <= split) {
        childGenes.add(mGenes.get(i));
        childGenes2.add(fGenes.get(i));
      }else{
        childGenes.add(fGenes.get(i));
        childGenes2.add(mGenes.get(i));
      }
    }
    
    ArrayList<Creature> offsprings = new ArrayList<Creature>();
    
    offsprings.add(new Creature(childGenes));
    offsprings.add(new Creature(childGenes2));

    return offsprings;
  }
}



class Creature implements Comparable<Creature> {
  private ArrayList<Character> genes = new ArrayList<Character>();
  private ArrayList<Node> nodes = new ArrayList<Node>();
  private Iterator<Character> itr;
  private Random rnd = new Random();
  private char currentDirection = 'a';
  private char newDirection = 'a';
  private boolean dead = false;
  private Food current;
  
  int alive;
  int score;
  
  Creature(ArrayList<Character> genes){
    for(int i = 20; i <= (nodeCount+1)*10; i += 10){
      nodes.add(new Node(50+i,80));
    }
    
    if(food)
    	newFood();
    
    if(genes == null){
      char[] gene = {'w','s','a','d'};
      
      for(int i=0;i<geneCount;i++){
        this.genes.add(gene[rnd.nextInt(4)]);
      }
      
    }else{
      this.genes.addAll(genes);
    }

    itr = this.genes.iterator();
  }
  
  public void move(){
    if(itr.hasNext()){
      newDirection = itr.next();
      
      switch(currentDirection){
        case 'w':
          if(newDirection != 's')
            currentDirection = newDirection;
          break;
          
        case 's':
          if(newDirection != 'w')
            currentDirection = newDirection;
          break;
          
        case 'a':
          if(newDirection != 'd')
            currentDirection = newDirection;
          break;
          
        case 'd':
          if(newDirection != 'a')
            currentDirection = newDirection;
          break;
      }
      
      if(!dead){
        alive++;
        ArrayList<int[]> cords = new ArrayList<int[]>();
    
        for(Node n : nodes){
          cords.add(n.getCords());
        }
        
        nodes.get(0).move(currentDirection);
        
        Iterator<int[]> itr = cords.iterator();
        Node prevN = null;
        
        for(Node n : nodes){
          if(n != nodes.get(0)){
            int[] prevCord = prevN.getCords();
            int[] cordSet = itr.next();
            
            if(!(prevCord[0] == cordSet[0] && prevCord[1] == cordSet[1]))
              n.setCords(cordSet);
          }
          prevN = n;
        }
        int colX = nodes.get(0).getCords()[0];
        int colY = nodes.get(0).getCords()[1];
        
        for(Node n : nodes){
          if(n != nodes.get(0)){
            int[] current = n.getCords();
            
            if(colX == current[0] && colY == current[1]){
                  this.dead = true;
            }
          }
        }
        
        int[] head = nodes.get(0).getCords();
        if(food){
		   int[] food = current.getCords();
		   
		   if(head[0] == food[0] && head[1] == food[1]){
		     addNode();
		     newFood();
		   }
        }
        if(head[0] < 20 || head[1] < 20 || head[0] > mapx || head[1] > mapy){
          dead = true;
        }
        
        if(dead){
         for(int i = 0; i<nodes.size();i++){
           nodes.get(i).setCords(cords.get(i));
         }
        }
      }
    }else{
      itr = genes.iterator();
      move();
    }
  }
  
  public void draw(){
    if(food)
    	current.draw();
    for(Node n : nodes){
      n.draw();
    }
  }
  
  public int compareTo(Creature other){
    //return other.getScore() - getScore();
    return other.getAlive() - this.alive;
    //return (other.getFitness() - this.getFitness());
  }
  
  public int getAlive(){
    return this.alive;
  }
  
  public int getScore(){
    return this.score;
  }
  
  public int getFitness(){
    return getScore() * getAlive();
  }
  
  public boolean isDead(){
    return this.dead;
  }
  
  public void kill(){
    score = 0;
    alive = 0;
    dead = true;
  }
  
  public void addNode(){
    int[] lastCords = nodes.get(nodes.size() - 1).getCords();
    nodes.add(new Node(lastCords[0],lastCords[1]));
    score++;
  }
   
  public ArrayList<Character> getGenes(){
    return this.genes;
  }
  
  public void mutate(){
    char[] gene = {'w','s','a','d'};
    //Collections.swap(genes, rnd.nextInt(genes.size()), rnd.nextInt(genes.size()));
    int index = rnd.nextInt(genes.size());
    genes.remove(index);
    genes.add(index, gene[rnd.nextInt(4)]);
    itr = genes.iterator();
  }
  
  public void newFood(){    
    int fx;
    int fy;
    
    while(true){
      fx = rnd.nextInt((mapx/10) - 1)+2;
      fy = rnd.nextInt((mapy/10) - 1)+2;
      
      fx *= 10;
      fy *= 10;
      
      boolean collide = false;
      
      for(Node n : nodes){
        int[] xy = n.getCords();
        
        if(xy[0] == fx && xy[1] == fy){
          collide = true;
          break;
        }
      }
      
      if (!collide)
        break;
    }
    current = new Food(fx,fy);
  }
}


class Node {
  private int x;
  private int y;
  
  Node(int x, int y){
    this.x = x;
    this.y = y;
  }
  
  public void setCords(int[] cords){
    this.x = cords[0];
    this.y = cords[1];
  }
  
  public int[] getCords(){
    int[] cords = {this.x,this.y};
    return cords;
  }
  
  public void move(char key){
     switch(key) {
      case 'w':
        this.y-=10;
        break;
        
      case 's':
        this.y+=10;
        break;
        
      case 'a':
        this.x-=10;
        break;
        
      case 'd':
        this.x+=10;
        break;
    }
  }
  
  public void draw(){
    stroke(256,256,256);
    fill(128,128,128);
    ellipse(x,y, 10, 10);
    fill(256,256,256);
  }
}

class Food {
  private int size;
  private int x;
  private int y;
  
  Food(int x,int y){
    this.x = x;
    this.y = y;
  }
  
  public int[] getCords(){
    int[] cords = {x,y};
    return cords;
  }
  
  public void draw(){
    stroke(256,256,256);
    fill(0,0,0);
    ellipse(x,y,10,10);
  }
}

























