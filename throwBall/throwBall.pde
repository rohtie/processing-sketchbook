ArrayList<Ball> balls = new ArrayList<Ball>();
Random rnd = new Random();

void setup(){
  size(700,500);
  smooth();
  stroke(256,256,0);
  frameRate(30);
  for(int i=0;i<256;i++){
    balls.add(new Ball((rnd.nextInt(20)+1) * rnd.nextFloat(),(-(rnd.nextInt(20)+1)) * rnd.nextFloat()));
  }
}

void draw(){
  background(0);
  
  for(Ball ba : balls){
    ba.move();
    ba.draw();
  }
}

class Ball {
  int dt = 1;
  float L = 0.9;
  float C = 0.0002;
  float g = -0.02;
  float M = 0.1;
  float x = 21;
  float y = 479;
  float vx;
  float vy;
  float fvx;
  float fvy;
  float times = 0.5;
  int r;
  int gr;
  int b;
  
  Ball(float vx,float vy){
    this.vx = vx;
    this.vy = vy;
    this.fvx = vx;
    this.fvy = vy;
    Random rnd = new Random();
    r = rnd.nextInt(256);
    gr = rnd.nextInt(256);
    b = rnd.nextInt(256);
  }
  
  void move(){
     if(x < 680 && x > 20 && y<480 && y > 20){
        float v = sqrt(vx*vx+vy*vy);
        float ax = (-C*L*v*vx)/M;
        float ay = (-C*L*v*vy)/M - g;
        vx += ax*dt;
        vy += ay*dt;
        x += vx*dt;
        y += vy*dt;
        
     }else{ 
       if(x>680){
         vx += (-1) * (fvx/times);
         x = 679;
       }
       
       if(x<20){
         vx += fvx/times;
         x=21;
       }
       
       if(y<20){
         vy += (-1) * (fvy/times);
         y=21;
       }
       
       if(y>480){
         vy += fvy/times;
         y=479;
       }
       times+=5;
     }
  }
  
  void draw(){
    stroke(0,0,0);
    fill(r,gr,b);
    ellipse(x,y,20,20);
  }
}
